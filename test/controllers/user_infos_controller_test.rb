require 'test_helper'

class UserInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_info = user_infos(:one)
  end

  test "should get index" do
    get user_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_user_info_url
    assert_response :success
  end

  test "should create user_info" do
    assert_difference('UserInfo.count') do
      post user_infos_url, params: { user_info: { city_1: @user_info.city_1, city_2: @user_info.city_2, country_1: @user_info.country_1, country_2: @user_info.country_2, district_1: @user_info.district_1, district_2: @user_info.district_2, first_name: @user_info.first_name, gender: @user_info.gender, land_mark_1: @user_info.land_mark_1, land_mark_2: @user_info.land_mark_2, last_name: @user_info.last_name, phone: @user_info.phone, secondary_email: @user_info.secondary_email, state_1: @user_info.state_1, state_2: @user_info.state_2, street_address_1: @user_info.street_address_1, street_address_2: @user_info.street_address_2, user_id: @user_info.user_id } }
    end

    assert_redirected_to user_info_url(UserInfo.last)
  end

  test "should show user_info" do
    get user_info_url(@user_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_info_url(@user_info)
    assert_response :success
  end

  test "should update user_info" do
    patch user_info_url(@user_info), params: { user_info: { city_1: @user_info.city_1, city_2: @user_info.city_2, country_1: @user_info.country_1, country_2: @user_info.country_2, district_1: @user_info.district_1, district_2: @user_info.district_2, first_name: @user_info.first_name, gender: @user_info.gender, land_mark_1: @user_info.land_mark_1, land_mark_2: @user_info.land_mark_2, last_name: @user_info.last_name, phone: @user_info.phone, secondary_email: @user_info.secondary_email, state_1: @user_info.state_1, state_2: @user_info.state_2, street_address_1: @user_info.street_address_1, street_address_2: @user_info.street_address_2, user_id: @user_info.user_id } }
    assert_redirected_to user_info_url(@user_info)
  end

  test "should destroy user_info" do
    assert_difference('UserInfo.count', -1) do
      delete user_info_url(@user_info)
    end

    assert_redirected_to user_infos_url
  end
end
