require "application_system_test_case"

class UserInfosTest < ApplicationSystemTestCase
  setup do
    @user_info = user_infos(:one)
  end

  test "visiting the index" do
    visit user_infos_url
    assert_selector "h1", text: "User Infos"
  end

  test "creating a User info" do
    visit user_infos_url
    click_on "New User Info"

    fill_in "City 1", with: @user_info.city_1
    fill_in "City 2", with: @user_info.city_2
    fill_in "Country 1", with: @user_info.country_1
    fill_in "Country 2", with: @user_info.country_2
    fill_in "District 1", with: @user_info.district_1
    fill_in "District 2", with: @user_info.district_2
    fill_in "First name", with: @user_info.first_name
    fill_in "Gender", with: @user_info.gender
    fill_in "Land mark 1", with: @user_info.land_mark_1
    fill_in "Land mark 2", with: @user_info.land_mark_2
    fill_in "Last name", with: @user_info.last_name
    fill_in "Phone", with: @user_info.phone
    fill_in "Secondary email", with: @user_info.secondary_email
    fill_in "State 1", with: @user_info.state_1
    fill_in "State 2", with: @user_info.state_2
    fill_in "Street address 1", with: @user_info.street_address_1
    fill_in "Street address 2", with: @user_info.street_address_2
    fill_in "User", with: @user_info.user_id
    click_on "Create User info"

    assert_text "User info was successfully created"
    click_on "Back"
  end

  test "updating a User info" do
    visit user_infos_url
    click_on "Edit", match: :first

    fill_in "City 1", with: @user_info.city_1
    fill_in "City 2", with: @user_info.city_2
    fill_in "Country 1", with: @user_info.country_1
    fill_in "Country 2", with: @user_info.country_2
    fill_in "District 1", with: @user_info.district_1
    fill_in "District 2", with: @user_info.district_2
    fill_in "First name", with: @user_info.first_name
    fill_in "Gender", with: @user_info.gender
    fill_in "Land mark 1", with: @user_info.land_mark_1
    fill_in "Land mark 2", with: @user_info.land_mark_2
    fill_in "Last name", with: @user_info.last_name
    fill_in "Phone", with: @user_info.phone
    fill_in "Secondary email", with: @user_info.secondary_email
    fill_in "State 1", with: @user_info.state_1
    fill_in "State 2", with: @user_info.state_2
    fill_in "Street address 1", with: @user_info.street_address_1
    fill_in "Street address 2", with: @user_info.street_address_2
    fill_in "User", with: @user_info.user_id
    click_on "Update User info"

    assert_text "User info was successfully updated"
    click_on "Back"
  end

  test "destroying a User info" do
    visit user_infos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User info was successfully destroyed"
  end
end
