class CreateUserInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :user_infos do |t|
      t.string :first_name
      t.string :last_name
      t.string :secondary_email
      t.string :gender
      t.string :user_id
      t.string :phone
      t.string :street_address_1
      t.string :land_mark_1
      t.string :city_1
      t.string :district_1
      t.string :state_1
      t.string :country_1
      t.string :street_address_2
      t.string :land_mark_2
      t.string :city_2
      t.string :district_2
      t.string :state_2
      t.string :country_2

      t.timestamps
    end
  end
end
