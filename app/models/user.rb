class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :blogs
  has_one :user_info
  after_create :create_user_info
  def create_user_info
    UserInfo.create_or_find_by(:user_id => self.id)
  end

end
