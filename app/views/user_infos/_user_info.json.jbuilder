json.extract! user_info, :id, :first_name, :last_name, :secondary_email, :gender, :user_id, :phone, :street_address_1, :land_mark_1, :city_1, :district_1, :state_1, :country_1, :street_address_2, :land_mark_2, :city_2, :district_2, :state_2, :country_2, :created_at, :updated_at
json.url user_info_url(user_info, format: :json)
