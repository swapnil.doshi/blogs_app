# frozen_string_literal: true
Rails.application.routes.draw do
  resources :user_infos
  resources :blogs
  devise_for :users
  root to: 'blogs#index'
end
